<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class developer extends Model
{
    public $timestamps = false;
    public $primaryKey = 'iddeveloper';
    protected $fillable = ['country'=>'string', 'developer_name'=> 'string'];
}
