<?php

namespace App\Http\Controllers;

use App\developer;
use App\Http\Requests;
use App\product;
use App\product_has_developer;
use Illuminate\Http\Request;

use DB;
use Input;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function details()
    {
        $id_product = Input::get('id_product');
        $results = DB::table('products')
            ->join('developers','products.developer_iddeveloper','=','iddeveloper')
            ->join('car_models','car_models.idcar_model','=','products.car_model_idcar_model')
            ->where('idproduct','=',$id_product)->first();

        return view('details',['product' => $results]);
    }

    public function index()
    {
        $results = DB::table('products')
                    ->join('developers','products.developer_iddeveloper','=','iddeveloper')
                    ->join('car_models','car_models.idcar_model','=','products.car_model_idcar_model')
                    ->get(array('product_name', 'developer_name','car_model_name','idproduct','price','iddeveloper','idcar_model'));
        return view('home',['pricelist' => $results]);
    }
}
