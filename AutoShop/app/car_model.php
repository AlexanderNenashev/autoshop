<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class car_model extends Model
{
    public $timestamps = false;
    public $primaryKey = 'idcar_model';
    protected $fillable = ['car_model_name'=>'string', 'developer_iddeveloper'=> 'int(11)'];
}
