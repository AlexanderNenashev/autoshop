<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{//price(int(11)), product_name(varchar(70))
    public $timestamps = false;
    public $primaryKey = 'idproduct';
    protected $fillable = ['price'=>'integer(11)', 'product_name'=>'string'];
}
