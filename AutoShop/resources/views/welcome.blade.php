@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="jumbotron">
                <h1>Здравствуйте!</h1>
                <p>Мы предлагаем Вам широкий выбор автозапчастей, начиная с Оки и заканчивая Bugatti</p>
                <p><a href="{{ url('/home') }}" class="btn btn-primary btn-lg" role="button">Прайс-лист</a></p>
            </div>
            <!--<div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    Your Application's Landing Page.
                </div>
            </div>-->
        </div>
    </div>
</div>
@endsection
