@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Вашему вниманию представлен прайс-лист</h4></div>

                <div class="panel-body">
                    <table class="table table-striped">

                        <tr>
                            <th>Название</th>
                            <th>Производитель</th>
                            <th>Машина</th>
                            <th>Цена</th>
                            <th></th>
                            <th></th>
                        </tr>

                        <?php foreach ($pricelist as $pricelist_item): ?>
                        <tr>
                            <td>
                                <?php echo $pricelist_item->product_name;?>
                            </td>
                            <td>
                                <?php echo $pricelist_item->developer_name; ?>
                            </td>
                            <td>
                                <?php echo $pricelist_item->car_model_name;?>
                            </td>
                            <td>
                                <?php echo $pricelist_item->price;?>
                            </td>
                            <td>
                                <a href="/home/details?id_product=<?php echo $pricelist_item->idproduct;?>" class="btn btn-info" role="button">Подробнее</a>
                            </td>
                            <td>
                                <a id="<?php echo $pricelist_item->idproduct;?>" href="#" class="btn btn-success bucket" role="button">Купить</a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<button id="btn1">jQuery</button>
<script src="js/jquery-2.2.3.js"></script>
<!--<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>-->
<script type="text/javascript">
    $(document).ready(function(){

        var href=$("a.bucket");
        var output=Array();
        href.click(function(){
            $(this).css('background-color', 'green');
            console.log((this).id);
            output.push((this).id);
//alert((this).id);
        });
        $("#btn1").click(function(){
//var a=output.first();
            console.log(output[0]);
            alert(output);
        });

    });
</script>
@endsection
