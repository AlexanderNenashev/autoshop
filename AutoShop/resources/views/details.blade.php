@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h4><?php echo $product->product_name;?></h4></div>
                <table class="table table-striped">

                    <tr>
                        <th>Название</th>
                        <th>Производитель</th>
                        <th>Машина</th>
                        <th></th>
                    </tr>

                    <tr>
                        <td>
                            <?php echo $product->product_name;?>
                        </td>
                        <td>
                            <?php echo $product->developer_name; ?>
                        </td>
                        <td>
                            <?php echo $product->car_model_name;?>
                        </td>
                    </tr>
                </table>
                <div class="panel-body">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
