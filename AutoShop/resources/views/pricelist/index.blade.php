@extends('app')
@section('content')

    <div>
        <table class="table table-striped table-bordered">
            <tr>
                <th>Name</th>
                <th>Price</th>
            </tr>

            <?php foreach ($pricelist as $pricelist_item): ?>
                <tr>
                    <td>
                        <?php echo $pricelist_item->product_name; ?>
                    </td>
                    <td>
                        <?php echo $pricelist_item->price;?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
@stop